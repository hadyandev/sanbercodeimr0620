import React from 'react';
import logo from './logo.svg';
// import './App.css';

function App() {
  return (
    <div className="App">
    <h1>Form Biodata</h1>
    <p>Silahkan isi form dibawah ini...</p>
    <label>Nama</label>
    <br/>
    <input id="nama" type="text" placeholder="Nama Anda"/>
    <br/>
    <label>Alamat</label>
    <br/>
    <textarea id="alamat" cols="30" rows="10" placeholder="Alamat Anda"></textarea>
    <br/>
    <label>Pekerjaan</label>
    <br/>
    <input id="pekerjaan" type="text" placeholder="Pekerjaan Anda"/>
    <br/>
    <label>Gaji</label>
    <br/>
    <input id="gaji" type="number" placeholder="Gaji Anda"/>
    <br/>
    <label>Tahun Masuk</label>
    <br/>
    <input id="tahun_masuk" type="date"/>
    </div>
  );
}

export default App;
