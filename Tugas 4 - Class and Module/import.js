import { NamedExport as AliasNamed, helloCat } from './export.js'
import User from './user.js'

const name = new AliasNamed('Hadyan Ahmad');
console.log(name);

const hadyan = new User('Hadyan');
console.log(hadyan);

console.log(helloCat());

