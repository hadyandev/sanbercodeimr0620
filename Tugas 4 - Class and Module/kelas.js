// Mengubah object menjadi kelas
class MakhlukGhaib {
    constructor(nama, kekuatan){
        this.nama = nama;
        this.kekuatan = kekuatan;
    }
    
    attack() {
        console.log('Senyum');
    }
};
  
class Benda{
    constructor(nama, bahan, actionName){
        this.nama = nama;
        this.bahan = bahan;
        this.actionName = actionName;
    }
    
    action() {
        console.log(this.actionName);
    }
};
  
class HalGhaibLain{
    constructor(nama, bahan, actionName){
        this.nama = nama;
        this.bahan = bahan;
        this.actionName = actionName;
    }
    
    action() {
        console.log(this.actionName);
    }
};

const kompor = new Benda('kompor', 'besi', 'memanaskan');
console.log(kompor);

const halGhaibLain = new HalGhaibLain('cinta', 'perjuangan bersama', 'membara');
console.log(halGhaibLain);

class Kendaraan extends Benda{
    constructor(nama, bahan, actionName){
        super(nama, bahan, actionName);
    }

    actionLain(){
        console.log("ini action lain");
    }
}

const mobil = new Kendaraan('mobil', 'alumunium', 'balapan');
console.log(mobil);
mobil.actionLain();