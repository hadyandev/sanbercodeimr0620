const mobil = [
    {
        merk_mobil: "Honda",
        plat_nomor: "B3431",
        tahun_produksi: "2013"
    },
    {
        merk_mobil: "Toyota",
        plat_nomor: "Z0790",
        tahun_produksi: "2017"
    },
    {
        merk_mobil: "Daihatsu",
        plat_nomor: "G8702",
        tahun_produksi: "2012"
    },
    {
        merk_mobil: "Suzuki",
        plat_nomor: "K1313",
        tahun_produksi: "2018"
    },
    {
        merk_mobil: "Mitsubishi",
        plat_nomor: "Y8703",
        tahun_produksi: "2016"
    },
    {
        merk_mobil: "Nissan",
        plat_nomor: "P3429",
        tahun_produksi: "2014"
    },
    {
        merk_mobil: "Ferrari",
        plat_nomor: "R3114",
        tahun_produksi: "2019"
    },
    {
        merk_mobil: "BMW",
        plat_nomor: "F1341",
        tahun_produksi: "2020"
    }
];

console.log(mobil);

const numbers = [1, 4, 5, 1, 2, 10, 12, 15, 11, 13, 11, 5];
// Menjumlahkan element array dengan value genap
console.log(numbers.reduce((accumulator, currentValue) => currentValue % 2 == 0 ? accumulator + currentValue : accumulator + 0, 0));

// Mengurutkan element array
console.log(numbers.sort((num1, num2) => num1 - num2));


let text = "Rever_1_Test_2";
const removeReverse = string => {
    string = string.replace(/e/g, "");
    let splitString = string.split("");
    let reverseString = splitString.reverse();
    joinString = reverseString.join("");
    return joinString;
}
console.log(removeReverse(text));

const ascending = list => {
    let length = list.length;
    
    for (let i = length-1; i >= 0; i--){
       for(let j = 1; j <= i; j++){
           if(list[j-1] > list[j]){
               let currentValue = list[j-1];
               list[j-1] = list[j];
               list[j] = currentValue;
            }
       }
    }
    return list;
}

console.log(ascending(numbers));